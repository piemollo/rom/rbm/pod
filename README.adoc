# Proper Orthogonal Decomposition
:Author:    Pierre Mollo
:Email:     pierre.mollo@univ-reims.fr
:Date:      2021-12-20
:Revision:  2021-12-20

## Dependances

* link:https://gitlab.com/piemollo/tb/elaf[Extended Linear Algebra for FreeFem]

## How to use it

This library is design for link:https://freefem.org/[`FreeFem v4.9`].
To use it, you need first to download this repository and use `include <path to dir>/POD.idp`. 
You will then have access to the following functions.

:WARNING: This library needs link:https://gitlab.com/piemollo/tb/elaf[Extended Linear Algebra FreeFem].

## Function list

* xref:POD[`POD`]: Computes the Proper Orthogonal Decomposition.

## Documentation

NOTE: All `POD(...)` functions are defined using overload

### POD (thresholded)[[POD]]

.Description
This function computes the Propre Orthogonal Decomposition of a given collection using a threshold to select the number of modes.

.Argument
* `real[int,int] u`: Collection of snapshots
* `real threshold`: Threshold for the method
* `matrix Xh`: (Optional) Inner product matrix
* `real[int] ceV`: (Optional) return computed eigen values

.Use
[source,C]
----
// ...
real[int,int] RB = POD(u, threshold, Xh, ceV);
real[int,int] RB = POD(u, threshold);
real[int,int] RB = POD(u, threshold, ceV);
real[int,int] RB = POD(u, threshold, Xh);
----

NOTE: When the inner product matrix `Xh` is not specified `POD` uses 
`eye` matrix.

### POD (enforced)[[POD]]

.Description
This function computes the Propre Orthogonal Decomposition of a given collection using an user-specified number of modes.

.Argument
* `real[int,int] u`: Collection of snapshots
* `int K`: Number of wanted modes
* `matrix Xh`: (Optional) Inner product matrix
* `real[int] ceV`: (Optional) return computed eigen values

.Use
[source,C]
----
// ...
real[int,int] RB = POD(u, K, Xh, ceV);
real[int,int] RB = POD(u, K);
real[int,int] RB = POD(u, K, ceV);
real[int,int] RB = POD(u, K, Xh);
----
