// ------ ------ Header
cout << "#===== ====== ====== ====== =====#" << endl;
cout << "|  Proper Orth. Decomposition    |" << endl;
cout << "|         Validation             |" << endl;
cout << "#===== ====== ====== ====== =====#" << endl;

// ------ Library
verbosity = 0;
include "POD.idp"

// --- Initiate
mesh Th = square(50,50);
fespace Vh(Th,P2);

varf vXh(u,v,tgv=-2) = int2d(Th)(dx(u)*dx(v)+dy(u)*dy(v)) + on(2, u=0);
matrix Xh = vXh(Vh,Vh);

// --- Manifold
real[int] mu(20); for(int i=0; i<mu.n; i++) mu[i] = (i+1)*2*pi;
real MU;
Vh u,v;
real[int,int] B(Vh.ndof, mu.n);

// --- Some problem
problem pb(u,v) = 
int2d(Th)(dx(u)*dx(v)*MU + dy(u)*dy(v))
+ int2d(Th)(v*3)
+ on(2,3,u=0);

for(int i=0; i<mu.n; i++){
	MU = mu[i];
	pb;
	plot(u, fill=1, cmm=""+i);
	B(:,i) = u[];
}

// ------ POD (thresholded)
cout << "| --- Thresholded POD " << endl;
real threshold=1e-6;
real[int] ceV;
real[int,int] RB = POD(B, threshold, Xh, ceV);
for(int i=0; i<RB.m; i++){
	u[] = RB(:,i);
	plot(u, fill=1, cmm="snap"+i, value=1);
}

cout << "| threshold:"+threshold << endl;
cout << "| nb of eigenmodes:"+RB.m << endl;

RB = POD(B, threshold);
cout << "| POD#1: eigenmodes="+RB.m <<endl;
RB = POD(B, threshold, Xh);
cout << "| POD#2: eigenmodes="+RB.m <<endl;
RB = POD(B, threshold, ceV);
cout << "| POD#3: eigenmodes="+RB.m <<endl;

// ------ POD (thresholded)
cout << "| --- Enforced POD " << endl;
int K=7;
real[int,int] RBk = POD(B, K, Xh, ceV);

cout << "| K:"+K << endl;
cout << "| nb of eigenmodes:"+RBk.m << endl;

RBk = POD(B, K);
cout << "| POD#1: eigenmodes="+RBk.m <<endl;
RBk = POD(B, K, Xh);
cout << "| POD#2: eigenmodes="+RBk.m <<endl;
RBk = POD(B, K, ceV);
cout << "| POD#3: eigenmodes="+RBk.m <<endl;

cout << "#===== ====== ====== ====== =====#" << endl;
